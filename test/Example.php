<?php
class Example extends PHPUnit_Extensions_SeleniumTestCase
{
  protected function setUp()
  {
    $this->setBrowser("firefox");
    $this->setBrowserUrl("https://bitbucket.org/");
  }

  public function testMyTestCase()
  {
    $this->open("/");
    $this->click("link=Log in");
    $this->waitForPageToLoad("30000");
    $this->type("id=id_username", "louisthaihv");
    $this->type("id=id_password", "hoangvanthai");
    $this->click("name=submit");
    $this->waitForPageToLoad("30000");
    $this->click("id=user-dropdown-trigger");
    $this->click("id=log-out-link");
    $this->waitForPageToLoad("30000");
  }
}
?>