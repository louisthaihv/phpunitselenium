<?php
	class MultiBrowserTest extends PHPUnit_Extensions_Selenium2TestCase {

		 // default params
	    public $parameters = array(
	        'seleniumServerRequestsTimeout' => 30000,
	        'timeout'                       => 30000,
	    );

	    // list of browsers with per-browser config
	    public static $browsers = array(
	        
	        array('browserName' => 'Safari on MacOS X',
	              'host'=>'10.0.0.9',
	              'port'=>4444)
	    );

	    protected function setUp()
	    {
	        $this->setBrowserUrl('http://www.google.com/');
	    }

	    function testTitle()
	    {
	        $this->url('/');
	        echo $this->getBrowser()."\n\r";      
	    }
	}