<?php
class TesLogin extends PHPUnit_Extensions_Selenium2TestCase {

	protected function setUp() {
		$this->setBrowser('firefox');
		$this->setBrowserUrl('http://kenshin.local.nadia/');
	}

	public function testHasLoginForm() {
		$this->url('login/staff');
		$userName = $this->byName('id');
		$password = $this->byName('pass');

		$this->assertEquals('', $userName->value());
		$this->assertEquals('', $password->value());
	}

	public function testLogIntoStaff(){
		$this->url('/login/staff');

		$form = $this->byCssSelector('form');
		
		$action = $form->attribute('action');
		//$this->assertEquals('http://kenshin.local.nadia/login/staff', $action);
		$this->assertContains('login/staff', $action);
		$this->byName('id')->value('doc1');
		$this->byName('pass')->value('pass');
		$form->submit();

		$wlcome = $this->byId('ScreenHeader_title')->text();
		$this->assertEquals('健診一覧', $wlcome);
		//$this->assertRegExp('/健診/i', string, 'message');
	}

	// public function testSubmitButtonIsDesabledUntilFieldsAreFilled() {
	// 	$this->url('login/staff');
	// 	$this->assertFalse($this->byCssSelector('btmbtn cursor')->enabled());

	// 	$this->byName('id')->value('doc1');
	// 	$this->byName('pass')->value('pass');

	// 	$this->assertTrue($this->byCssSelector('btmbtn cursor')->enabled());
	// }
}