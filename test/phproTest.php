<?php
class phproTest extends PHPUnit_Extensions_Selenium2TestCase{
	protected function setUp(){
		$this->setBrowser('firefox');
		$this->setBrowserUrl('http://www.phpro.org');
	}
	public function testTitle(){
		$title = 'PHP Tutorials Examples phPro - Tutorials Articles Examples Development';
		$this->url('http://www.phpro.org');
		$this->assertEquals($title, $this->title());
	}
}