<?php
class kenshinTest extends PHPUnit_Extensions_Selenium2TestCase{
    protected function setUp(){
        $this->setBrowser('firefox');
        $this->setBrowserUrl('http://kenshin.local.nadia/');
    }

    public function testTitle(){
        $this->url('http://kenshin.local.nadia/');
        $this->assertEquals('Example WWW Page', $this->title());
    }

}
